#!/bin/bash
SCRIPT_PATH="/home/ablaikhans/Scripts/ssh_config"
SCRIPT_NAME=open_servers.sh
SSH_CONFIG=~/.ssh/config
ACTION=`echo $1 | awk '{print tolower($0)}' `
ACTIONS=("info" "get-groups" "get-group-hosts" "ssh-group-hosts" "run-command")

# show info
if [ $ACTION == "info" ] || [ -z $ACTION ] || [ $ACTION == "--info" ] || [ $ACTION == "--help" ] || [ $ACTION == "-h" ]
 then
    echo "[INFO] Run this script with parameter \"ACTION\""
    echo "[INFO] Available actions: ${ACTIONS[@]}"
fi

# GET server groups and its lines
SERVER_GROUP_NAMES=()
SERVER_GROUP_LINES=()
SERVER_GROUPS_COUNT=0
for SERVER_GROUP in `grep -n "###" $SSH_CONFIG | awk -F":###" '{print $2 ":" $1}' | awk '{print tolower($0)}'  `
do
    echo $((++SERVER_GROUPS_COUNT)) >/dev/null
        # get all groups from ssh config
    # set group name
    SERVER_GROUP_NAME=`echo $SERVER_GROUP | awk -F":" '{print $1}'`
    # set group line
    SERVER_GROUP_LINE=`echo $SERVER_GROUP | awk -F":" '{print $2}'`
    SERVER_GROUP_NAMES+=("$SERVER_GROUP_NAME")
    SERVER_GROUP_LINES+=("$SERVER_GROUP_LINE")

done

#echo "SERVER_GROUPS_COUNT: $SERVER_GROUPS_COUNT"

# Configure ACTION "get-groups"
if [ $ACTION == "get-groups" ]
then
    for (( i=0; i<$SERVER_GROUPS_COUNT; i++ ))
    do
        echo "Group \"${SERVER_GROUP_NAMES[$i]}\" in line ${SERVER_GROUP_LINES[$i]} "
    done
fi

# Configure ACTION "get-group-hosts"
if [ $ACTION == "get-group-hosts" ]
    then GROUP_NAME=`echo $2 | awk '{print tolower($0)}' `
fi

if [ $ACTION == "get-group-hosts" ] && [ -z $GROUP_NAME ]
then
    echo "[ERROR] Run \"get-group-hosts\" action with parameter \"group_name\": \"$SCRIPT_PATH/$SCRIPT_NAME get-group_hosts group_name\""
    echo "[ERROR] Run this script with parameter get-groups to get the list of group_names \"$SCRIPT_PATH/$SCRIPT_NAME get-groups\""
else if [ $ACTION == "get-group-hosts" ] && [ -z $3 ]
    then
        echo "[INFO] Getting hosts list of a group \"$GROUP_NAME\""
        Line_start=0
        Line_end=0
        for (( i=0; i<$SERVER_GROUPS_COUNT; i++ ))
        do
            if [ ${SERVER_GROUP_NAMES[$i]} == $GROUP_NAME ]
            then
                Line_start=${SERVER_GROUP_LINES[$i]}
                Line_end=${SERVER_GROUP_LINES[$i+1]}
                Lines_count=$(( Line_end - Line_start -1 ))
                cat ~/.ssh/config | sed -n "$Line_start"',$p' | head -n $Lines_count | grep "^Host " | awk '{print $2}'
                echo ""
            fi
        done
        #echo "Line_start: $Line_start"
        #echo "Line_end: $Line_end"
        #echo "Lines_count: $Lines_count"
    fi
fi

# Configure ACTION "ssh-group-hosts"
if [ $ACTION == "ssh-group-hosts" ]
    then GROUP_NAME=`echo $2 | awk '{print tolower($0)}' `
fi

if [ $ACTION == "ssh-group-hosts" ] && [ -z $GROUP_NAME ]
then
    echo "[ERROR] Run \"ssh-group-hosts\" action with parameter \"group_name\": \"$SCRIPT_PATH/$SCRIPT_NAME get-group_hosts group_name\""
    echo "[ERROR] Run this script with parameter get-groups to get the list of group_names \"$SCRIPT_PATH/$SCRIPT_NAME get-groups\""
else if [ $ACTION == "ssh-group-hosts" ] && [ -z $3 ]
    then
        echo "[INFO] Getting hosts list of a group \"$GROUP_NAME\""
        Line_start=0
        Line_end=0
        for (( i=0; i<$SERVER_GROUPS_COUNT; i++ ))
        do
            if [ ${SERVER_GROUP_NAMES[$i]} == $GROUP_NAME ]
            then
                Line_start=${SERVER_GROUP_LINES[$i]}
                Line_end=${SERVER_GROUP_LINES[$i+1]}
                Lines_count=$(( Line_end - Line_start -1 ))
                SSH_CONNECT_GROUP_HOSTS=`cat ~/.ssh/config | sed -n "$Line_start"',$p' | head -n $Lines_count | grep "^Host " | awk '{print $2}'` gnome-terminal --working-directory="." -- bash -c '
                    for host in $SSH_CONNECT_GROUP_HOSTS
                    do
                        gnome-terminal --tab --working-directory="." -- bash -c "ssh $host"
                    done'
            fi
        done
    fi
fi